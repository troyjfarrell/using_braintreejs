import base64
import sys

from Crypto.Cipher import AES, PKCS1_v1_5
from Crypto.Hash import SHA256
from Crypto.Hash.HMAC import HMAC
from Crypto.PublicKey import RSA

class RsaDecryptionFailedError(Exception):
    """
    The decryption of the keys failed.
    """

class HmacIsInvalidError(Exception):
    """
    The HMAC of a value is invalid.
    """

def Decrypt(rsaCipher, encryptedValue):
    """
    Decrypt Braintree.js-encrypted values

    rsaCipher - a PKCS#1 v1.5 RSA cipher object from PyCrypto (which contains
                the necessary private key information)
    encryptedValue - the encrypted string from braintree.js
    """

    # Get the version, keys, ciphertext and hmac from the user's input.
    parts = filter(None, encryptedValue.split('$'))
    version, keys, ciphertext, hmac = parts

    # The goal is to decrypt the ciphertext.  In order to do that, we need to
    # verify the HMAC, because it is best practice to authenticate ciphertext
    # before decrypting it.  To verify the HMAC, we need the HMAC key, which
    # is encrypted with our public key in the ``keys`` variable (along with the
    # AES key.)  So we have these steps:
    #
    # 1) Decrypt ``keys`` with our private key, and extract the AES and HMAC
    #    keys from it.
    # 2) Verify the HMAC(ciphertext, HMAC key) matches.  If not, report the
    #    error and discontinue the process.
    # 3) Decrypt the ciphertext and return it.

    # Step one: decrypt the keys with our private key.
    keysBytes = base64.b64decode(keys)
    keysDecrypted = rsaCipher.decrypt(keysBytes, None)
    if keysDecrypted is None:
        raise RsaDecryptionFailedError()
    keysDecoded = base64.b64decode(keysDecrypted) 
    aesKey, hmacKey = keysDecoded[:32], keysDecoded[32:]

    # Step two: verify the HMAC.
    hmacCruncher = HMAC(hmacKey, base64.b64decode(ciphertext), SHA256)
    decodedHmac = base64.b64decode(hmac)
    if hmacCruncher.digest() != decodedHmac:
        raise HmacIsInvalidError()

    # Step three: decrypt the ciphertext.
    decodedCiphertext = base64.b64decode(ciphertext)
    initializationVector = decodedCiphertext[:16] 
    ciphertextWithoutIV = decodedCiphertext[16:]
    aesCipher = AES.new(key=aesKey, mode=AES.MODE_CBC, IV=initializationVector)
    plaintext = aesCipher.decrypt(ciphertextWithoutIV)
    return plaintext
    
if 2 != len(sys.argv):
    print("Usage: {0} <private_key.pem>")
    sys.exit(1)

privatePem = sys.argv[1]
with open(privatePem) as privateKeyFile:
    privateKey = RSA.importKey(privateKeyFile.read())
rsaCipher = PKCS1_v1_5.new(privateKey)

while True:
    try:
        encryptedValue = raw_input("Encrypted value: ")
        print(Decrypt(rsaCipher, encryptedValue.strip()))
    except HmacIsInvalidError:
        print("The HMAC value is invalid.")
    except (EOFError, KeyboardInterrupt):
        print()
        break
    except RsaDecryptionFailedError:
        print("Failed to decrypt keys.")
    except ValueError:
        print("Unable to interpreter this value.")
