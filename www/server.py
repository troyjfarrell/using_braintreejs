from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import smtplib

import flask

app = flask.Flask(__name__)
app.debug = True
app.secret_key = 'ndMdGYskhvfRNtXdmBPnYGdHdZRZ9EvrDvvjaCtNcT8EqZAs'

gmail_user = 'user@gmail.com'
gmail_pwd = 'password'

@app.route('/')
def index():
    return flask.render_template('index.html')

@app.route('/contact', methods=['POST'])
def contact():
   text = 'From: {email}\n\n{message}'.format(
    email=flask.request.form['email'],
    message=flask.request.form['message'],
   )

   msg = MIMEMultipart()

   msg['From'] = gmail_user
   msg['To'] = gmail_user
   msg['Subject'] = 'Contact Form Submission'

   msg.attach(MIMEText(text))

   smtp = smtplib.SMTP('smtp.gmail.com', 587)
   smtp.ehlo()
   smtp.starttls()
   smtp.ehlo()
   smtp.login(gmail_user, gmail_pwd)
   smtp.sendmail(gmail_user, gmail_user, msg.as_string())
   smtp.quit()
   smtp.close()
   return 'Message sent!'

if __name__ == '__main__':
    app.run()
