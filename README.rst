Using Braintree.js
==================

This is a demonstration of how you can use braintree.js_ to encrypt and decrypt
information submitted through your HTML forms.  For a walk-through, please read
`Securing Your Forms With Braintree.js`_.

The private keys in this repository are known to be public.

.. _braintree.js: https://github.com/braintree/braintree.js
.. _`Securing Your Forms With Braintree.js`: http://blog.artlogic.com/2013/06/05/securing-your-forms-with-braintree-js/
